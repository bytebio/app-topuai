import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { MateriaPage } from '../pages/materia/materia';
import { SettingsPage } from '../pages/settings/settings';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { CategoriasPage } from '../pages/categorias/categorias';
import { CidadesPage } from '../pages/cidades/cidades';
import { TermoPagePage } from '../pages/termo/termo';
import { PoliticaPagePage } from '../pages/politica/politica';
import { OfflinePage } from '../pages/offline/offline';
import { CartaoPage } from '../pages/cartao/cartao';
import { GetCidadePage } from '../pages/get-cidade/get-cidade';
import { PromocoesPage } from '../pages/promocoes/promocoes';
import { CriarcontaPage } from '../pages/criarconta/criarconta';
import { PromocaoPage } from '../pages/promocao/promocao';
import { Network } from '@ionic-native/network';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextMaskModule } from 'angular2-text-mask';
import { AppVersion } from '@ionic-native/app-version';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Badge } from '@ionic-native/badge';
import { OneSignal } from '@ionic-native/onesignal';

// const cloudSettings: CloudSettings = {
//   'core': {
//     'app_id': '1004887a'
//   }
// };


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    MateriaPage,
    SettingsPage,
    LoginPage,
    RegisterPage,
    CategoriasPage,
    CidadesPage,
    OfflinePage,
    CartaoPage,
    PromocoesPage,
    CriarcontaPage,
    PromocaoPage,
    GetCidadePage,
    TermoPagePage,
    PoliticaPagePage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    TextMaskModule,
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    MateriaPage,
    SettingsPage,
    LoginPage,
    RegisterPage,
    CategoriasPage,
    CidadesPage,
    OfflinePage,
    CartaoPage,
    PromocoesPage,
    CriarcontaPage,
    PromocaoPage,
    GetCidadePage,
    TermoPagePage,
    PoliticaPagePage
  ],
  providers: [{
    provide:
      ErrorHandler,
      useClass: IonicErrorHandler
    },
    Storage,
    Network,
    GoogleAnalytics,
    SocialSharing,
    AppVersion,
    Badge,
    OneSignal
  ]
})

export class AppModule {}
