import { Component, ViewChild } from '@angular/core';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AppVersion } from '@ionic-native/app-version';
import { Nav, Platform, MenuController, Events, App, AlertController } from 'ionic-angular';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { SettingsPage } from '../pages/settings/settings';
import { LoginPage } from '../pages/login/login';
import { OfflinePage } from '../pages/offline/offline';
import { PromocoesPage } from '../pages/promocoes/promocoes';
import { RegisterPage } from '../pages/register/register';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Network } from '@ionic-native/network';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Badge } from '@ionic-native/badge';
import { OneSignal } from '@ionic-native/onesignal';
import { enableProdMode } from '@angular/core';
import 'rxjs/add/operator/map';

// enableProdMode();

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  public rootPage: any = TabsPage;

  errorPage = OfflinePage;

  @ViewChild(Nav) nav: Nav;

  pages: Array<{title: string, component: any,icon1: string, icon2: string, cor: string, hasSubMenu:boolean, showSub:boolean, data:any, visible:any}>;

  private urlcidades: string = "http://live-topuai.pantheonsite.io/rest/cidades";

  private urlcategorias: string = "http://live-topuai.pantheonsite.io/rest/categorias";

  private url_logout: string = "http://live-topuai.pantheonsite.io/user/logout?_format=json&token=";

  cidades: Array<{title: string, id: any, active: any, tid: any, categorias: any, hasSubMenu: boolean, icon2: string }>;

  categorias: Array<{data: any}>;

  public sessao = null;

  public cidade = null;

  public nome =null;

  public avatar =null;

  public version = null;

  constructor(private app:App,
              public platform: Platform,
              public menuCtrl: MenuController,
              public http: Http,
              public storage: Storage,
              public events: Events,
              private network: Network,
              private ga: GoogleAnalytics,
              public alertCtrl: AlertController,
              private appVersion: AppVersion,
              private badge: Badge,
              private oneSignal: OneSignal
              ) {

    this.http.get(this.urlcidades)//BUSCA CIDADES
      .map(res => res.json())
      .subscribe(
      data => {
        this.storage.set('cidades', data).then(() => {
          this.cidades=data;
          // data.unshift({"tid":[{"value":0}],"name":[{"value":"Geral"}],"description":[{"value":null,"format":null}]});//ADD TODOS
          this.events.publish('menu:itens', data);
        });
      },
      error => {
        console.log('error cidades');
        this.nav.setRoot(this.errorPage);

    });
    this.http.get(this.urlcategorias)//BUSCA CATEGORIAS
    .map(res => res.json())
    .subscribe(
      data => {
        // data.unshift({"tid":[{"value":0}],"name":[{"value":"Geral"}],"description":[{"value":null,"format":null}]});//ADD TODOS
        this.categorias=data;
        this.events.publish('menu:itens', data);
      },
      error => {
        console.log('error categorias');
        this.nav.setRoot(this.errorPage);
    });

    this.storage.get('cidade').then((val) => {
      this.cidade=val;
      this.events.publish('menu:itens', val);
    });

    this.events.subscribe('menu:itens', (data) => {//chama o toogleMenu();
      if(this.cidade!=null && this.cidade!=null &&  typeof(this.cidades) != "undefined" ){
        for (var i = this.cidades.length - 1; i >= 0; i--) {
          if(this.cidade.tid["0"].value==this.cidades[i].tid["0"].value){
            this.cidades[i].active=1;//marca cidade ativa
          }else{
            this.cidades[i].active=0;
          }
        }
      }
      this.toggleMenu();
    });
    // this.events.publish('onesignalRefreshUser', {});
    this.events.subscribe('onesignalRefreshUser', () => {//evento para atualizar user;
      this.onesignalSetTagsUser();
    });

    // this.events.publish('onesignalRefreshCidade', {});
    this.events.subscribe('onesignalRefreshCidade', () => {//evento para atualzar cidade
      this.onesignalSetCidade();
    });
    this.network.onDisconnect().subscribe(() => {//verifica se desconectou
      console.log('network was disconnected :-(');
      this.nav.push(OfflinePage);
    });


    this.network.onConnect().subscribe(() => {//verifica se conectou
      console.log('network connected!'); 
      this.nav.pop();
    });

    platform.ready().then(() => {

      if(this.platform.is('cordova')) {//RODA SE FOR CORDOVA

        this.appVersion.getVersionNumber().then(data => {//PEGA VERSAO DO APP
          this.version=data;//add version ao app
        });

        this.ga.startTrackerWithId('UA-97726623-1')//INICIA O GOOGLE ANALITYCS
        .then(() => {
          this.ga.trackView('Acesso ao App');
          this.ga.setAppVersion(this.version);
          // this.ga.debugMode();
          // this.ga.setAllowIDFACollection(true);
         // Tracker is ready
         // You can now track pages or set additional information such as AppVersion or UserId
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));
      }//FIM DO IS PLATFORM

      console.log('XXXX19');

      this.initPushNotification();//INICIALIZA NOTIFICACAO

      this.badge.clear();//limpa badge do app;
    });
  }



  initializeApp() {

    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();

    });
  }
  openPage(page,data=null)   {

    if(page.hasSubMenu==true){
      if(page.title=='Portal TopUai'){//menu topuai
        this.nav.push(TabsPage,[{data,page,topuai:1}]);
        this.menuCtrl.close();
      }else if(page.title=='Cidades'){//abre menu cidades
        this.toggleDetails(page);
      }else if(page.vid["0"].target_id=="cidades"){//vai por cidades
        this.nav.push(TabsPage,[{data,page,only_cidade:1}]);
        this.menuCtrl.close();
      }
    }else{

      if(page.title=="Sair"){
        this.logout();
      }else if(page.title=="Cartão"){
          this.nav.push(TabsPage,[{data,page,tab_select:3}]);
          this.menuCtrl.close();
      }else if(page.title=="Descontos"){
          this.nav.push(TabsPage,[{data,page,tab_select:2}]);
          this.menuCtrl.close();
      }else if(typeof(page.tid) != "undefined"){
        var cidade=({"tid":[{"value":'all'}],"name":[{"value":"TopUai"}],"description":[{"value":null,"format":null}]});
        this.nav.push(TabsPage,[{cidade,page,filter_categoria:1}]);
        this.menuCtrl.close();
      }else{
        this.menuCtrl.close();
        this.nav.setRoot(page.component);
      }
    }
  }
  openPageCidade(page,cidade) {//ABRE PAGE CIDADES
    this.menuCtrl.close();
    this.nav.push(TabsPage,[{cidade,page,filter_categoria:1}]);
  }
  openProfilePage(){
    this.menuCtrl.close();
    this.nav.push(RegisterPage,[{edit:1}]);
  }
  logout(){//funcao para logout de user.

    this.storage.get('sessao_login').then((val) => {


      let headers = new Headers();
      headers.append("Content-Type", "Content-type: application/json");
      headers.append("X-CSRF-Token", val.logout_token);
      let options = new RequestOptions({ headers: headers});
      this.http.post(this.url_logout+val.logout_token, options)
      .map(res => res.json())
      .subscribe(
        data => {
          // console.log(data);
        },
        error => {
          console.log(error);
      });
    });
     this.storage.set('sessao', null).then(() => {
        this.storage.set('cidade', null);
        this.storage.set('sessao_login', null);
        this.menuCtrl.close();
        this.sessao=null;
        this.cidade=null;
        this.events.publish('menu:itens',[{}]);
        this.app.getRootNav().setRoot(TabsPage);
      });
  }
  toggleMenu() {//ATUALIZA O MENU

    if(this.cidade==null){//BUSCA CIDADES
      this.storage.get('cidade').then((val) => {
        this.cidade=val;
      });
    }

    if(typeof(this.cidades) != "undefined" ){
      for (var i = 0; i < this.cidades.length; i++) {
        this.cidades[i].categorias=this.categorias;
        this.cidades[i].hasSubMenu=true;
        this.cidades[i].icon2='add';
      }
    }

    this.storage.get('sessao').then((val) => {//verifica se tem sessao

      this.sessao=val;

      if(this.sessao!=null){//ADD NOME E AVATAR DO USER NO MENU
        if(typeof(this.sessao.user_picture["0"]) != "undefined" ){
          this.avatar=this.sessao.user_picture["0"].url;
        }else{
          this.avatar=0;
        }
        if(typeof(this.sessao.field_full_name["0"]) != "undefined" ){
          this.nome=this.sessao.field_full_name["0"].value;
        }
      }

      this.pages = [{
        title: 'Home',
        component: TabsPage,
        icon1: 'home',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:1
        },{
        title: 'Portal TopUai',
        component: TabsPage,
        icon1: 'play',
        icon2: 'add-circle',
        cor:'#ffffff',
        hasSubMenu: true,
        showSub: false,
        data: this.categorias,
        visible:1
        },{
        title: 'Cidades',
        component: false,
        icon1: 'pin',
        icon2: 'add-circle',
        cor:'#ffffff',
        hasSubMenu: true,
        showSub: false,
        data: this.cidades,
        visible:1
        },{
        title: 'Cartão',
        component: TabsPage,
        icon1: 'card',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:(this.sessao==null ? false:true)
        },{
        title: 'Descontos',
        component: PromocoesPage,
        icon1: 'star',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:1
        },{
        title: 'Sobre',
        component: AboutPage,
        icon1: 'information-circle',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:0
        },{
        title: 'Configurações',
        component: SettingsPage,
        icon1: 'settings',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:1
        },{
        title: 'Contato',
        component: ContactPage,
        icon1: 'contact',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:1
        },{
        title: 'Entrar',
        component: LoginPage,
        icon1: 'log-in',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:(this.sessao==null ? true:false)
        },{
        title: 'Sair',
        component: false,
        icon1: 'exit',
        icon2: 'arrow-forward',
        cor:'#ffffff',
        hasSubMenu: false,
        showSub: false,
        data:null,
        visible:(this.sessao==null ? false:true)
      }];
    });
  }
  toggleDetails(page) {
    if(page.showDetails) {
      page.showDetails = false;
      if(page.icon2=='remove'){
        page.icon2 = 'add';
      }else{
        page.icon2 = 'add-circle';
      }
    } else {
      page.showDetails = true;
      if(page.icon2=='add'){
        page.icon2 = 'remove';
      }else{
        page.icon2 = 'remove-circle';
      }
    }
  }


  notificationOpenedCallback(data){
    console.log('open Callback');
    this.nav.setRoot(TabsPage,{materia_nid:data});
  }

  //Inicia o Push
  initPushNotification() {


    if (!this.platform.is('cordova')) {
      console.log("Push notifications not initialized. Cordova is not available - Run in physical device");
      return;
    }

    this.oneSignal.startInit('2c18848e-5639-40f0-a9bc-366f3cbf98c8', '1056302448691');

    this.onesignalSetTagsUser();//seta User
    this.onesignalSetCidade();//seta Cidade

    this.storage.get('cidade').then((val) => {
      if(val!=null){
        this.oneSignal.deleteTag('cidade');
        this.oneSignal.sendTag('cidade', val.name["0"].value);
      }
    });

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe((data: any) => {
      // console.log(data);
      console.log('NOTIFICATION Received!');
      // console.log('handleNotificationReceived: ' + JSON.stringify(data));
    });
    this.oneSignal.handleNotificationOpened().subscribe((data: any) => {
      // console.log('handleNotificationOpened: ' + JSON.stringify(data));
      console.log('NOTIFICATION OPENEDN!');

      if(typeof(data) != "undefined" &&
         typeof(data.notification) != "undefined" &&
         typeof(data.notification.payload) != "undefined" &&
         typeof(data.notification.payload.additionalData) != "undefined" &&
         typeof(data.notification.payload.additionalData.materia_nid) != "undefined" ){
          // console.log('entrou no if!');
          this.notificationOpenedCallback(data.notification.payload.additionalData.materia_nid);
      }
    });
    this.oneSignal.endInit();
  }

//seta cidade default
  onesignalSetCidade(){
    this.storage.get('cidade').then((val) => {
      if(val!=null){
        this.oneSignal.deleteTag('cidade');
        this.oneSignal.sendTag('cidade', val.name["0"].value);
      }
    });
  }

  // Seta dados do usuario.
  onesignalSetTagsUser(){
    this.storage.get('sessao').then((val) => {//verifica se tem sessao
       if(val!=null){
        this.oneSignal.deleteTags(['email', 'login', 'uid', 'sexo', 'nome','bairro_user','cidade_user','uf_user','cidade_interesse','editorias_interesse']);
        this.oneSignal.sendTag('email', val.mail["0"].value);
        this.oneSignal.sendTag('login', val.name["0"].value);
        this.oneSignal.sendTag('uid', val.uid["0"].value);
        if(val.field_user_sexo.length>0){
          this.oneSignal.sendTag('sexo', val.field_user_sexo["0"].value);
        }
        if(val.field_full_name.length>0){
          this.oneSignal.sendTag('nome', val.field_full_name["0"].value);
        }
        if(val.field_user_endereco.length>0 && val.field_user_endereco["0"].administrative_area!=''){
          this.oneSignal.sendTag('bairro_user', val.field_user_endereco["0"].dependent_locality);
        }
        if(val.field_user_endereco.length>0 && val.field_user_endereco["0"].locality!=''){
          this.oneSignal.sendTag('cidade_user', val.field_user_endereco["0"].locality);
        }
        if(val.field_user_endereco.length>0 && val.field_user_endereco["0"].administrative_area!=''){
          this.oneSignal.sendTag('uf_user', val.field_user_endereco["0"].administrative_area);
        }

        if(val.field_user_info_cidade.length>0){
          var cidade='';
          var aux_cidade='';
          for (var i = 0; i < val.field_user_info_cidade.length; i++) {
            aux_cidade=val.field_user_info_cidade[i].url;
            cidade=cidade+' '+aux_cidade.substr(1);
          }
          this.oneSignal.sendTag('cidade_interesse', cidade);
        }

        if(val.field_user_info_cidade.length>0){
          var editorias='';
          var aux_editorias='';
          for (var i2 = 0; i2 < val.field_user_info_editorias.length; i2++) {
            aux_editorias=val.field_user_info_editorias[i2].url;
            editorias=editorias+' '+aux_editorias.substr(1);
          }
          this.oneSignal.sendTag('editorias_interesse', editorias);
        }
      }
    });
  }
}


