import { Component } from '@angular/core';
import { Nav, NavController, NavParams, ToastController, Events, AlertController,LoadingController,ModalController} from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { TermoPagePage } from '../../pages/termo/termo';
import { PoliticaPagePage } from '../../pages/politica/politica';
import 'rxjs/add/operator/map';
import { LoginPage } from '../../pages/login/login';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

/*
  Generated class for the Register page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-register',
    templateUrl: 'register.html'
  })

  export class RegisterPage {

    formRegister: any;

    pass: any;

    public mask = [/\d/, /\d/, /\d/,'.',/\d/, /\d/, /\d/,'.',/\d/, /\d/, /\d/, '-',/\d/, /\d/];

    pass2: any;

    pass3: any;

    sessao_login_aux:any;

    labelsalvar:string = 'Cadastre-se';

    labelpass:string = 'Senha';

    labelpass2:string = 'Confirme a senha';

    public edit = 0;

    uid: any;

    public loading: any;

    private url: string = 'http://live-topuai.pantheonsite.io/user/register?_format=json';

    constructor(public navCtrl: NavController,
                public storage: Storage,
                public navParams: NavParams,
                private formBuilder: FormBuilder,
                public toastCtrl: ToastController,
                public alertCtrl: AlertController,
                public http: Http,
                public nav: Nav,
                public events: Events,
                public loadingCtrl: LoadingController,
                private ga: GoogleAnalytics,
                public modalCtrl: ModalController) {

      this.ga.trackView('Registro');

      this.formRegister = this.formBuilder.group({
        nome: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        login: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        email: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        pass: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        pass2: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        pass3: ['', Validators.compose([Validators.minLength(3)])],
        nasc:['', Validators.compose([Validators.required, Validators.minLength(3)])],
        cidade:['', Validators.compose([Validators.required, Validators.minLength(3)])],
        cpf:['', Validators.compose([Validators.required, Validators.minLength(14)])],
        termo:[''],
        uf:[''],
      });

      if(typeof(navParams.data["0"]) != "undefined" &&
        typeof(navParams.data["0"].edit) != "undefined"&&
        navParams.data["0"].edit==1){
        this.edit=1;
        this.labelpass = 'Nova Senha';
        this.labelsalvar = 'Atualizar';
        this.labelpass2= 'Confirme Nova Senha';
        this.getUser();
      }
    }

    registerForm(){//SUBMIT DO FORM DE REGISTRO


      if(this.edit==1){
        this.sendUser_validate();//vai para edit;
      }else{
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
        if (this.formRegister.dirty && this.formRegister.valid) {
          // console.log();

          // if(this.formRegister.value.termo==false){
          //   let toast = this.toastCtrl.create({
          //     message: 'Você deve aceitar o termo para se cadastrar!',
          //     duration: 3000,
          //   });

          //   toast.present();

          // }else

          if(this.formRegister.value.pass!=this.formRegister.value.pass2){
            let toast = this.toastCtrl.create({
              message: 'As senhas não conferem!',
              duration: 3000,
            });

            toast.onDidDismiss(() => {
              this.pass = this.formRegister.controls[ 'pass' ];
              this.pass.setValue( '' );

              this.pass2 = this.formRegister.controls[ 'pass2' ];
              this.pass2.setValue( '' );
            });
            toast.present();
          }else{

            let headers = new Headers();
            headers.append("Content-Type", 'application/json');
            let options = new RequestOptions({ headers: headers });


            let cpf=this.formRegister.value.cpf;
            cpf= cpf.slice(0, 14);
            this.http.post(this.url,
              {"name":[{"value":this.formRegister.value.login}],
               "mail":[{"value":this.formRegister.value.email}],
               "pass":this.formRegister.value.pass,
               "field_full_name": [{"value":this.formRegister.value.nome}],
               "field_cpf": [{"value":cpf}],
               "field_user_nascimento": [{"value":this.formRegister.value.nasc}],
               "field_user_endereco": [{
                  "langcode": "pt-br",
                  "country_code": "BR",
                  "administrative_area": this.formRegister.value.uf,
                  "locality": this.formRegister.value.cidade,
                  "dependent_locality": "",
                  "postal_code": "",
                  "sorting_code": null,
                  "address_line1": "",
                  "address_line2": "",
                  "organization": "",
                  "given_name": "",
                  "additional_name": null,
                  "family_name": ""
                }
              ],
              }, options)
              .map(res => res.json())
              .subscribe(
              data => {
                this.loading.dismiss().catch(() => {});//FECHA O LOADING

                let toast2 = this.toastCtrl.create({
                  message: 'Cadastro realizado com sucesso! Efetuando o seu login...',
                  duration: 1800,
                });

                toast2.onDidDismiss(() => {
                  this.nav.setRoot(LoginPage,{'login':this.formRegister.value.login,'password':this.formRegister.value.pass});
                });

                toast2.present();
              },
              error => {
                console.log(error._body);
                let aux = error._body;

                this.loading.dismiss().catch(() => {});//FECHA O LOADING
                aux=aux.replace('{"message":"', '');
                aux=aux.replace('"}', '');

                if(error._body=='{"message":"Unprocessable Entity: validation failed.\nname: The username '+this.formRegister.value.login+' is already taken.\n"}'){
                  aux='O Login '+this.formRegister.value.login+' já está em uso.';
                }else if( error._body=='{"message":"Unprocessable Entity: validation failed.\nmail: O endere\u00e7o de '+this.formRegister.value.email+' j\u00e1 est\u00e1 cadastrado.\n"}'){
                  aux='O e-mail '+this.formRegister.value.email+' já está em uso.';
                }

                let alert = this.alertCtrl.create({
                  title: 'Ops2!',
                  subTitle: aux,
                  buttons: ['OK']
                });
                alert.present();
              });
          }
        }
      }
    }

    getUser(){//carrega dados do usuario

      this.loading = this.loadingCtrl.create({});
      this.loading.present();
      this.storage.get('sessao').then((val_sessao) => {
        this.storage.get('sessao_login').then((val_login) => {


          this.sessao_login_aux=val_login;

          let headers = new Headers();
          headers.append("Content-Type", 'application/json');
          headers.append("Authorization", this.sessao_login_aux.basic);
          headers.append("X-CSRF-Token", this.sessao_login_aux.csrf_token);

          let options = new RequestOptions({ headers: headers });
          this.http.get('http://live-topuai.pantheonsite.io/user/'+this.sessao_login_aux.current_user.uid+'?_format=json' , options)
          .map(res => res.json())
          .subscribe(
          data => {

            this.storage.set('sessao', data).then(() => {
              this.events.publish('onesignalRefreshUser', {});//atualiza dados do onesignal do usuario
              this.events.publish('menu:itens',[{}]);
            });

            this.formRegister = this.formBuilder.group({
              nome: [data.field_full_name["0"].value, Validators.compose([Validators.required, Validators.minLength(3)])],
              login: [data.name["0"].value, Validators.compose([Validators.required, Validators.minLength(3)])],
              email: [data.mail["0"].value, Validators.compose([Validators.required, Validators.minLength(3)])],
              pass: ['', Validators.compose([ Validators.minLength(3)])],
              pass2: ['', Validators.compose([ Validators.minLength(3)])],
              pass3: ['', Validators.compose([ Validators.minLength(3)])],
              nasc:[data.field_user_nascimento["0"].value, Validators.compose([Validators.required, Validators.minLength(3)])],
              cidade:[data.field_user_endereco["0"].locality, Validators.compose([Validators.required, Validators.minLength(3)])],
              cpf:[data.field_cpf["0"].value, Validators.compose([Validators.required, Validators.minLength(14)])],
              uf:[data.field_user_endereco["0"].administrative_area],
              termo:[''],
            });

            this.loading.dismiss().catch(() => {});//FECHA O LOADING
          },
          error => {
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
          });
        });
      });
    }

    sendUser_validate(){//valida dados do usuario antes do update

      this.loading = this.loadingCtrl.create({});
      this.loading.present();

      if(this.formRegister.value.pass=='' && this.formRegister.value.pass2==''&&this.formRegister.value.pass3==''){
        //não tem senhas para atualizar;
        this.sendUser(1);
      }else{// verifica se é pra mudar senhas

        if(this.formRegister.value.pass3==''){//verifica se colocou senha atual
          let toast = this.toastCtrl.create({
            message: 'Necessário informar a senha atual!',
            duration: 2300,
          });
          this.loading.dismiss().catch(() => {});//FECHA O LOADING
          toast.present();//fim

        }else if(btoa(this.formRegister.value.pass3)!=this.sessao_login_aux.pass){// verifica senha antiga
          let toast = this.toastCtrl.create({
            message: 'Senha atual incorreta!',
            duration: 2300,
          });
          this.loading.dismiss().catch(() => {});//FECHA O LOADING
          toast.present();
        }else if(this.formRegister.value.pass==''||this.formRegister.value.pass2==''){// verifica senha antiga
          let toast = this.toastCtrl.create({
            message: 'Necessário informar a nova senha!',
            duration: 2300,
          });
          this.loading.dismiss().catch(() => {});//FECHA O LOADING
          toast.present();//fim
        }else{
          if(this.formRegister.value.pass!=this.formRegister.value.pass2){
            let toast = this.toastCtrl.create({
              message: 'As senhas não conferem!',
              duration: 2300,
            });
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
            toast.present();//fim
          }else{
            this.sendUser(2);//manda para updete com senha
          }
        }
      }
    }

    sendUser(senhas){//manda update

      var dados={};

      if(senhas==1){//sem senha
        var cpf=this.formRegister.value.cpf;
        cpf= cpf.slice(0, 14);

      dados={"name":[{"value":this.formRegister.value.login}],
         "mail":[{"value":this.formRegister.value.email}],
         "field_full_name": [{"value":this.formRegister.value.nome}],
         "field_cpf": [{"value":cpf}],
         "field_user_nascimento": [{"value":this.formRegister.value.nasc}],
         "field_user_endereco": [{
            "langcode": "pt-br",
            "country_code": "BR",
            "administrative_area": this.formRegister.value.uf,
            "locality": this.formRegister.value.cidade,
            "dependent_locality": "",
            "postal_code": "",
            "sorting_code": null,
            "address_line1": "",
            "address_line2": "",
            "organization": "",
            "given_name": "",
            "additional_name": null,
            "family_name": ""
            }
          ],
        };
      }else{// com senha
        this.sessao_login_aux.pass=btoa(this.formRegister.value.pass);//atualiza para a nova senha
        let cpf=this.formRegister.value.cpf;
            cpf= cpf.slice(0, 14);

        dados={"name":[{"value":this.formRegister.value.login}],
           "mail":[{"value":this.formRegister.value.email}],
           "pass":[{
              "existing": this.formRegister.value.pass3,
              "value": this.formRegister.value.pass}],
           "field_full_name": [{"value":this.formRegister.value.nome}],
           "field_cpf": [{"value":cpf}],
           "field_user_nascimento": [{"value":this.formRegister.value.nasc}],
           "field_user_endereco": [{
              "langcode": "pt-br",
              "country_code": "BR",
              "administrative_area": this.formRegister.value.uf,
              "locality": this.formRegister.value.cidade,
              "dependent_locality": "",
              "postal_code": "",
              "sorting_code": null,
              "address_line1": "",
              "address_line2": "",
              "organization": "",
              "given_name": "",
              "additional_name": null,
              "family_name": ""
            }
          ],
        };
      }

      let headers = new Headers();
      headers.append("Content-Type", 'application/json');
      headers.append("Authorization", this.sessao_login_aux.basic);
      headers.append("X-CSRF-Token", this.sessao_login_aux.csrf_token);
      let options = new RequestOptions({ headers: headers });
      this.http.patch('http://live-topuai.pantheonsite.io/user/'+this.sessao_login_aux.current_user.uid+'?_format=json',dados , options)
      .map(res => res.json())
      .subscribe(
      data => {
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        let toast2 = this.toastCtrl.create({
          message: 'Dados atualizados com sucesso!',
          duration: 2500,
        });
        // this.reLogin();//
        toast2.onDidDismiss(() => {
          this.getUser();
        });
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        toast2.present();
      },
      error => {
        let aux = error._body;
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        aux=aux.replace('{"message":"', '');
        aux=aux.replace('"}', '');
        let alert = this.alertCtrl.create({
          title: 'Ops!',
          subTitle: aux,
          buttons: ['OK']
        });
        alert.present();
      });
    }

    reLogin(){

      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      let options = new RequestOptions({ headers: headers });

      this.http.post("http://live-topuai.pantheonsite.io/user/login?_format=json", {
      "name": this.formRegister.value.login,
      "pass": atob(this.sessao_login_aux.pass),
      "form_id": 'user_login_form'
      }, options)
      .map(res => res.json())
      .subscribe(
      data => {
        data.basic="Basic " + btoa(this.formRegister.value.login + ":" + atob(this.sessao_login_aux.pass));
        data.pass= this.sessao_login_aux.pass;
        this.storage.set('sessao_login', data).then(() => {

        });
      },
      error => {
        let aux = error._body;
        aux=aux.replace('{"message":"', '');
        aux=aux.replace('"}', '');
        let alert = this.alertCtrl.create({
          title: 'Ops!',
          subTitle: aux,
          buttons: ['OK']
        });
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        alert.present();
      });
    }


    openTermo(){
      let profileModal = this.modalCtrl.create(TermoPagePage);
      profileModal.present();
    }


    openPolitica(){
      let profileModal = this.modalCtrl.create(PoliticaPagePage);
      profileModal.present();
    }
  }
