import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

/*
  Generated class for the Promocao page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-promocao',
  templateUrl: 'promocao.html'
})
export class PromocaoPage {

  public promocao: Array<string>;

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private ga: GoogleAnalytics) {
    this.promocao = navParams.data;

    console.log(this.promocao);
    this.ga.trackView('Desconto - '+navParams.data.title);
    this.breadcrumb1=navParams.data.field_empresa_cidade;
  }
}
