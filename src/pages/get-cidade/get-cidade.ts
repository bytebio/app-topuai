import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,Events,ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the GetCidade page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-get-cidade',
  templateUrl: 'get-cidade.html'
})
export class GetCidadePage {

  public cidades: any;

  private url: string = "http://live-topuai.pantheonsite.io/rest/cidades";

  public loading: any;

  formCidades = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http,
              public viewCtrl: ViewController,
              private formBuilder: FormBuilder,
              public loadingCtrl: LoadingController,
              public events: Events,
              public storage: Storage) {
    this.loading = this.loadingCtrl.create({});
    this.loading.present();
    this.http.get(this.url)
    .map(res => res.json())
    .subscribe(data => {
      this.cidades = data;
      this.loading.dismiss();
    });
  }

  cidadeForm(){//SALVA CIDADE
    var aux_cidade=[];
    for (var i = 0; i < this.cidades.length; i++) {
      if(this.cidades[i].tid["0"].value==this.formCidades){
        aux_cidade=this.cidades[i];
        break;
      }
    }
    this.storage.set('cidade', aux_cidade).then(() => {
      this.events.publish('menu:itens', {});
      this.events.publish('onesignalRefreshCidade', {});
      this.viewCtrl.dismiss({'modal':'reload'});
    });
  }
}
