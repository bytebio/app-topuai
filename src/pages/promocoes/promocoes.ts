import { Component } from '@angular/core';
import { Nav, NavController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { OfflinePage } from '../../pages/offline/offline';
import { PromocaoPage } from '../../pages/promocao/promocao';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-promocoes',
  templateUrl: 'promocoes.html'
})
export class PromocoesPage {

  errorPage = OfflinePage;

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  public promocoes:any;

  promocao_page=PromocaoPage;

  searchQuery:any;

  items:any;

  loading:any;

  private url: string = "http://live-topuai.pantheonsite.io/rest/promocoes";

  constructor( public navCtrl: NavController,
                public http: Http,
                public loadingCtrl: LoadingController,
                public nav: Nav,
                private ga: GoogleAnalytics) {

    this.breadcrumb1="Descontos";

    this.ga.trackView('Descontos');

    this.loading = this.loadingCtrl.create({});
    this.loading.present();

    this.http.get(this.url)
    .map(res => res.json())
    .subscribe(
      data => {
        this.promocoes =data;
        console.log(this.promocoes);
        this.initializeItems();
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        this.nav.setRoot(this.errorPage);
    });
  }
  initializeItems() {
    this.items=this.promocoes;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return ((item.title.toLowerCase().indexOf(val.toLowerCase()) > -1)||//filtro por title
                (item.field_empresa_segmento.toLowerCase().indexOf(val.toLowerCase()) > -1)||//filtro por segmento
                (item.field_empresa_cidade.toLowerCase().indexOf(val.toLowerCase()) > -1));//filtro por cidade
      })
    }
  }

}
