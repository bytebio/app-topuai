import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

/*
  Generated class for the Cartao page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cartao',
  templateUrl: 'cartao.html'
})
export class CartaoPage {

  public nome:string = "";

  public cidade:string = "";

  public uf:string = "";

  public img:any;

  public cpf:string = "";

  public data:string = "";

  public id:string = "";

  public membro:string = "";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public storage: Storage,
              private ga: GoogleAnalytics) {
    this.ga.trackView('Cartão');
  }

  ionViewDidEnter() {

      this.storage.get('sessao').then((val) => {

      if(typeof(val.field_full_name["0"]) != "undefined" ){
        this.nome=val.field_full_name["0"].value;
      }
      if(typeof(val.field_user_endereco["0"]) != "undefined" ){
        this.cidade=val.field_user_endereco["0"].locality;
      }
      if(typeof(val.field_user_endereco["0"]) != "undefined" ){
        this.uf=val.field_user_endereco["0"].administrative_area;
      }
      if(typeof(val.user_picture["0"]) != "undefined" ){
        this.img=val.user_picture["0"].url;
      }else{
        this.img=0;
      }
      if(typeof(val.field_cpf["0"]) != "undefined" ){
        this.cpf=val.field_cpf["0"].value;
      }
      if(typeof(val.field_user_nascimento["0"]) != "undefined" ){

        var aux_data=val.field_user_nascimento["0"].value;

        aux_data=aux_data.split('-');

        this.data=aux_data[2]+'/'+aux_data[1]+'/'+aux_data[0];
      }
      if(typeof(val.uid["0"]) != "undefined" ){

        var id_length= new String(val.uid["0"].value);

        for (var i = 7; i > id_length.length; i--) {
          this.id=this.id+'0';
        }

        this.id=this.id+val.uid["0"].value;
      }

      if(typeof(val.created["0"]) != "undefined" ){
        var date = new Date(val.created["0"].value* 1000);
        var year = date.getFullYear();
        var month = ("0"+(date.getMonth()+1)).substr(-2);
        this.membro= month+'/'+year;
      }
    });
  }
}

