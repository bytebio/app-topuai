import { Component } from '@angular/core';
import { Nav, NavController, NavParams, AlertController, Events,LoadingController } from 'ionic-angular';
import { RegisterPage } from '../../pages/register/register';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { TabsPage } from '../../pages/tabs/tabs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  registerPage=RegisterPage;

  public Params : any;

  formLogin = {
    login: '',
    password: ''
  };

  private url: string = "http://live-topuai.pantheonsite.io/user/login?_format=json";

  constructor(public navCtrl: NavController,
              public nav: Nav,
              public navParams: NavParams,
              public http: Http,
              public alertCtrl: AlertController,
              public storage: Storage,
              public events: Events,
              public loadingCtrl: LoadingController,
              private ga: GoogleAnalytics) {

    this.ga.trackView('Login');

    if(typeof(navParams.data.login) != "undefined" && typeof(navParams.data.password) != "undefined"){
      this.formLogin=navParams.data;
      this.loginForm();
    }
  }

  loginForm(){

    let loading = this.loadingCtrl.create({});
    loading.present();

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    this.http.post(this.url, {
      "name": this.formLogin.login,
      "pass": this.formLogin.password,
      "form_id": 'user_login_form'
      }, options)
    .map(res => res.json())
    .subscribe(
      data => {
        data.basic="Basic " + btoa(this.formLogin.login + ":" + this.formLogin.password);
        data.pass= btoa(this.formLogin.password);
        this.storage.set('sessao_login', data).then(() => {
         let headers = new Headers();
          headers.append("Authorization", "Basic " + btoa(this.formLogin.login + ":" + this.formLogin.password));
          headers.append("Content-Type", "application/x-www-form-urlencoded");
          let options = new RequestOptions({ headers: headers });
           this.http.get("http://live-topuai.pantheonsite.io/user/"+data.current_user.uid+"?_format=json", options)
          .map(res => res.json())
          .subscribe(
            data => {
              this.storage.set('sessao', data).then(() => {
                this.events.publish('menu:itens',[{}]);
                this.events.publish('onesignalRefreshUser', {});//atualiza dados do onesignal do usuario
                loading.dismiss().catch(() => {});//FECHA O LOADING
                this.nav.setRoot(TabsPage);
              });
            },
            error => {
              let aux = error._body;
              console.log(error);
              aux=aux.replace('{"message":"', '');
              aux=aux.replace('"}', '');
              this.events.publish('menu:itens',[{}]);
              let alert = this.alertCtrl.create({
                title: 'Ops!',
                subTitle: aux,
                buttons: ['OK']
              });
            alert.present();
            this.formLogin = {
              login: '',
              password: ''
            };
          });
        });
      },
      error => {
        console.log(error);
        let aux = error._body;
        aux=aux.replace('{"message":"', '');
        aux=aux.replace('"}', '');
        this.events.publish('menu:itens',[{}]);

        if(error._body=='{"message":"Sorry, unrecognized username or password."}'){
          aux='Nome de usuário e senha não conferem.';
        }

        let alert = this.alertCtrl.create({
          title: 'Ops!',
          subTitle: aux,
          buttons: ['OK']
        });
      loading.dismiss().catch(() => {});//FECHA O LOADING
      alert.present();
      this.formLogin = {
        login: '',
        password: ''
      };
    });
  }
}
