import { Component,ViewChild } from '@angular/core';
import { Nav, NavController, NavParams, Tabs  } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CartaoPage } from '../cartao/cartao';
import { CriarcontaPage } from '../criarconta/criarconta';
import { PromocoesPage } from '../promocoes/promocoes';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;

  tab2Root: any = HomePage;

  tab3Root: any = PromocoesPage;

  tab4Root: any = CartaoPage;

  public ParamsData: any;

  public sessao = null;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public nav: Nav,
              public storage: Storage) {

    this.ParamsData=navParams.data;

    this.storage.get('sessao').then((val) => {//verifica se tem sessao
      this.sessao=val;
      if(this.sessao==null){
        this.tab4Root=CriarcontaPage;
      }
    });
  }

  ionViewDidEnter() {
    if(typeof(this.ParamsData["0"]) != "undefined"&&
    typeof(this.ParamsData["0"].tab_select) != "undefined" ){
      this.tabRef.select(this.ParamsData["0"].tab_select);
      console.log(this.ParamsData["0"].tab_select);
    }
  }
}
