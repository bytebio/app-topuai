import { Component } from '@angular/core';
import { Nav, NavController, LoadingController,ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { TabsPage } from '../../pages/tabs/tabs';
import { GoogleAnalytics } from '@ionic-native/google-analytics';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  cidade:0;
  notificacoes:0;

  public cidades: any;

  private url: string = "http://live-topuai.pantheonsite.io/rest/cidades";

  constructor(public navCtrl: NavController,
              public nav: Nav,
              public http: Http,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public toastCtrl: ToastController,
              public events: Events,
              private ga: GoogleAnalytics) {

    this.ga.trackView('Configurações');
    let loading = this.loadingCtrl.create({});

    loading.present();

    this.http.get(this.url)
    .map(res => res.json())
    .subscribe(data => {

      data.unshift({"tid":[{"value":0}],"name":[{"value":"Todas"}],"description":[{"value":null,"format":null}]});//ADD TODOS

      this.cidades = data;
      loading.dismiss();
    });

    storage.get('cidade').then((val) => {
      if(val!=null){
        this.cidade=val.tid["0"].value;
      }
    });

     storage.get('notificacoes').then((val) => {
      this.notificacoes=val;
    });
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Configurações atualizadas!',
      duration: 1400
    });

    toast.onDidDismiss(() => {
      this.nav.push(TabsPage);
    });

    toast.present();
  }

  saveCidade(value){//SALVA CIDADE

    var aux_cidade=[];
    for (var i = 0; i < this.cidades.length; i++) {
      if(this.cidades[i].tid["0"].value==value){
        aux_cidade=this.cidades[i];
        break;
      }
    }
    this.storage.set('cidade', aux_cidade).then(() => {
      this.storage.set('cidade', aux_cidade).then(() => {
        this.events.publish('menu:itens', {});
        this.events.publish('onesignalRefreshCidade', {});
        this.presentToast();
      });
    });

  }
  saveNotificacao(value){// SALVA NOTIFICACAO
    this.storage.set('notificacoes', value);
    this.presentToast();
  }

}
