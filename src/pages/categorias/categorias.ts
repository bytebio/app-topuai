import { Component } from '@angular/core';
import { NavController, LoadingController, Nav } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { OfflinePage } from '../../pages/offline/offline';
import { TabsPage } from '../../pages/tabs/tabs';

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html'
})
export class CategoriasPage {

  pageCategorias:any;

  public categorias: Array<string>;

  errorPage = OfflinePage;

  public breadcrumb: any;

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  public cidade: any;

  private url: string = "http://live-topuai.pantheonsite.io/rest/categorias";

  constructor(public navCtrl: NavController, public http: Http,public loadingCtrl: LoadingController, public storage: Storage, public nav: Nav) {

    let loading = this.loadingCtrl.create({});
    loading.present();
    this.pageCategorias=TabsPage;
    storage.get('cidade').then((val) => {//PEGA A CIDADE SELECIONADA
      this.cidade=val;
      if(val!=null&&val.name["0"].value!='Todas'){//MONTA URL COM CIDADE
        this.breadcrumb1=val.name["0"].value;
      }
      this.http.get(this.url)
      .map(res => res.json())
      .subscribe(data => {
        this.categorias = data;
        loading.dismiss();//FECHA O LOADING
      },
      error => {
        //FECHA O LOADING
        loading.dismiss().catch(() => {});//FECHA O LOADING
        this.nav.setRoot(this.errorPage);
      });
    });



  }
}
