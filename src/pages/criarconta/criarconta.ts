import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../../pages/register/register';
import { LoginPage } from '../../pages/login/login';

/*
  Generated class for the Criarconta page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-criarconta',
  templateUrl: 'criarconta.html'
})
export class CriarcontaPage {

  loginPage=LoginPage;
  registerPage=RegisterPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

}
