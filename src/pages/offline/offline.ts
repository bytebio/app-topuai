import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Network } from '@ionic-native/network';

/*
  Generated class for the Offline page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-offline',
  templateUrl: 'offline.html'
})
export class OfflinePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private network: Network,
              private ga: GoogleAnalytics) {
    this.ga.trackView('Offline');
  }

  doRefreshOffLine(refresher){

    // watch network for a connection

    let toast = this.toastCtrl.create({
      message: 'Verifique sua conexão com a rede!',
      duration: 1400,
    });

    toast.present();

    setTimeout(() => {

      refresher.complete();
    }, 1600);
  }
}
