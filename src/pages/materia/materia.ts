import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, NavParams, LoadingController,Platform,Content } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { OfflinePage } from '../../pages/offline/offline';
import { Http } from '@angular/http';
import { Slides } from 'ionic-angular';
import 'rxjs/add/operator/map';
import * as $ from 'jquery'
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'page-materia',
  templateUrl: 'materia.html'

})
export class MateriaPage {

  @ViewChild(Content) content: Content;

  @ViewChild(Slides) slides: Slides;

  public materia: any;

  public nid: any;

  public title:string = "";

  public created_1= "";

  public uid:string = "";

  public comment_count:string = "";

  public field_tags:string = "";

  public bodyHtmlString:any;

  public bodyHtmlStringSumario:string = "";

  public commentHtmlStringSumario:string = "";

  public comments:string = "";

  public facebookShare:string = "";

  public cidade:string = "";

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  public mensagem:string = "";

  public galeria:string = "";

  public image:string = "";

  public image1:string = "";

  public url:string = "";

  public galeria_original:string = "";

  public loading: any;

  public cidade_id: any= "all";

  public editoria_id: any= "all";

  public materias_destaque_semana: any;

  private urlMateria: string = "http://live-topuai.pantheonsite.io/rest/materia/";

  private urlVcGostarDe: string = "http://live-topuai.pantheonsite.io/rest/voce_gostar_de";

  constructor(public navCtrl: NavController,
              public params: NavParams,
              public storage: Storage,
              public nav: Nav,
              public http: Http,
              public loadingCtrl: LoadingController,
              private ga: GoogleAnalytics,
              public platform: Platform,
              private sanitized: DomSanitizer,
              private sharingVar: SocialSharing) {

    this.loading = this.loadingCtrl.create({});

    this.loading.present();

    if(typeof(params.data.materia_nid) != "undefined"){ // busca materia do server

      this.http.get(this.urlMateria+params.data.materia_nid)
      .map(res => res.json())
      .subscribe(
        data => {

          this.materia = data["0"];

          this.montaMateria();
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        this.nav.setRoot(OfflinePage);
        console.log('error materia');
      });

    }else{//pega materia do param
      this.materia = params.data;
      this.montaMateria();
    }

  }

  getVoceGostarde(){

    var url=this.urlVcGostarDe;

    if(this.cidade_id!=''){
      url=url+'/'+this.cidade_id;
    }else{
      url=url+'/all';
    }

    if(this.editoria_id!=''){
      url=url+'/'+this.editoria_id;
    }else{
      url=url+'/all';
    }

    //inicio destaques da semana
    this.http.get(url)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_destaque_semana=[];
          for (var i = 0; i < data.length; i++) {
            if(this.nid!=data[i].nid){//so add matéria diferentes.
              this.materias_destaque_semana.push(data[i]);
            }
          }
          if(this.materias_destaque_semana.length==0){
            this.materias_destaque_semana=null;
          }
      },
      error => {
        this.nav.setRoot(OfflinePage);
      }
    );
    //fim destaques da semana
  }


  shareTwitter() {//SHARE TWITTER
    // console.log('twitter');
    var msg=this.mensagem;
    msg=msg.substring(0,130);
    window.open('http://twitter.com/share?text='+msg+'&url='+this.url+'&hashtags=TopUai', '_system', 'location=yes');
  }

  shareFacebook() {//SHARE FACEBOOK
    // console.log('facebook');
    window.open('https://www.facebook.com/sharer/sharer.php?u='+this.url,'_system', 'location=yes');
  }

  Share(){//SHARE GERAL DO SISTEMA.
    this.loading = this.loadingCtrl.create({});
    this.sharingVar.share(this.mensagem, 'TopUai - '+this.materia.title, this.image, this.url).then(() => {
      this.loading.dismiss().catch(() => {});//FECHA O LOADING
    }).catch(() => {
      this.loading.dismiss().catch(() => {});//FECHA O LOADING
    });
  }
  montaMateria(){

    var publicidade ="<div id='ads-content-middle' class='advertisement'><span class='label'>CONTINUA DEPOIS DA PUBLICIDADE</span><hr><span class='publicidade publicidade_corpo'></span><hr></div>";

    this.breadcrumb1=this.materia.field_url_cidade_1;

    this.breadcrumb2=this.materia.field_category;

    this.materia.body=this.materia.body.replace(/src="\/sites\/default\/files\//gi,'src="https://topuai.com/sites/default/files/');

    var NewBody='';

    var res = this.materia.body.split("</p>");

    for (var i = 0; i < res.length; i++) {
      if(i==1){
        NewBody+=res[i]+publicidade;
      }else{
        NewBody+=res[i];
      }
    }

    this.title=this.materia.title;

    this.created_1=this.materia.created_1;

    this.cidade_id=this.materia.field_url_cidade;

    this.editoria_id=this.materia.field_category_1;

    this.uid=this.materia.uid;

    this.nid=this.materia.nid;

    this.comment_count=this.materia.comment_count;

    this.bodyHtmlString=this.transform(NewBody);

    this.bodyHtmlStringSumario=this.materia.body_1;

    this.commentHtmlStringSumario=this.materia.comment;

    this.mensagem=this.materia.body_1;

    this.image=this.materia.field_image;

    this.image1=this.materia.field_image_1;

    this.field_tags=this.materia.field_tags;

    this.image=this.image.replace(/http:\/\/live-topuai.pantheonsite.io/gi,'https://topuai.com');

    this.image1=this.image1.replace(/http:\/\/live-topuai.pantheonsite.io/gi,'https://topuai.com');

    if(this.materia.field_galeria!=''){

      this.materia.field_galeria=this.materia.field_galeria.replace(/http:\/\/live-topuai.pantheonsite.io/gi,'https://topuai.com');

      var galeria=this.materia.field_galeria.split(",");
      var galeria1=this.materia.field_galeria_1.split(",");

      for (var i = 0; i < galeria.length; i++) {
        galeria[i]=galeria[i].trim();
        galeria1[i]=galeria1[i].trim();
      }
      this.galeria=galeria;
      this.galeria_original=galeria1;
    }

    this.url=this.materia.path;

    this.url=this.url.replace(/http:\/\/live-topuai.pantheonsite.io/gi,'https://topuai.com');

    var comentarios='<div class="fb-comments" data-href="'+this.url+'" data-numposts="5" data-mobile="1 data-width="340"></div>';

    var share='<div class="fb-like" data-href="'+this.url+'" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>';

    this.comments=comentarios;

    this.facebookShare=share;

    this.ga.trackView('Matéria - '+this.materia.title);

    this.loading.dismiss().catch(() => {});//FECHA O LOADING

    this.getVoceGostarde();//monta matérias relacionadas;

    var cidade_aux=this.materia.field_url_cidade;//seta cidade para publicidade
      if(cidade_aux==''){
        cidade_aux='all';
      }
    var editoria_aux=this.materia.field_category_1;//seta editoria para publicidade
    if(editoria_aux==''){
      editoria_aux='all';
    }

    var randN=Math.floor((Math.random()*6)+1000);

    setTimeout(function(){
      $('.publicidade_corpo').empty().append("<iframe id='a574ba7a' name='a574ba7a' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=16&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='300' height='250'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a62c80b1&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=16&amp;cb="+randN+"&amp;n=a62c80b1&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
      // Zona ML-E. Mobile Leaderboard Interno 1
      $('.zone30').empty().append("<iframe id='aa94292b' name='aa94292b' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=30&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a0399a55&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=30&amp;cb="+randN+"&amp;n=a0399a55&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
      //Zona AB-C. Arroba Interno 1
      $('.zone19').empty().append("<iframe id='ac99a1d3' name='ac99a1d3' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=19&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='300' height='250'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=aefcd6fb&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=19&amp;cb="+randN+"&amp;n=aefcd6fb&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
      //Zona AB-D. Arroba Interno 2
      $('.zone20').empty().append("<iframe id='a421058a' name='a421058a' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=20&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='300' height='250'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=ae60ab74&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=20&amp;cb="+randN+"&amp;n=ae60ab74&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
      //Zona ML-A. Mobile Leaderboard Rodapé
      $('.zone15interna').empty().append("<iframe id='a0938ba7' name='a0938ba7' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=15&amp;cb="+randN+"&amp;front=0&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a30e37f0&amp;cb="+randN+"&amp;front=0&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=15&amp;cb="+randN+"&amp;front=0&amp;n=a30e37f0&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
    }, 1200);
  }

  nextSlide(self: any = this): void {
    self.slides.slidePrev();
  }
  prevSlide(self: any = this): void {
    self.slides.slideNext();
  }
  getMateria(materia){//volta para home com parametro para nova matéria.
    this.loading = this.loadingCtrl.create({});
    this.loading.present();
    this.materia = materia;
    this.montaMateria();
    this.content.scrollToTop();
    this.loading.dismiss().catch(() => {});//FECHA O LOADING
  }

  transform(value) {//aplica o sanitize
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
