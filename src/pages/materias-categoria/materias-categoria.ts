import { Component } from '@angular/core';
import { Nav, NavController, NavParams,LoadingController, AlertController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { MateriaPage } from '../../pages/materia/materia';

@Component({
  selector: 'page-materias-categoria',
  templateUrl: 'materias-categoria.html'
})
export class MateriasCategoriaPage {

  public categoria: Array<string>;

  public materias: Array<string>;

  public HasMaterias: boolean = true;

  materiapage = MateriaPage;

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  public cidadeTid:string = "";

  public categoriaTid:string = "";

  private url: string = "http://live-topuai.pantheonsite.io/rest/materias-categoria";

  constructor(public navCtrl: NavController, public navParams: NavParams,public nav: Nav,public http: Http,public loadingCtrl: LoadingController, public alertCtrl: AlertController, public events: Events) {
    let loading = this.loadingCtrl.create({});
    loading.present();
    if(navParams.data["0"].cidade.tid["0"].value==0){
      this.cidadeTid='all';
    }else{
      this.cidadeTid=navParams.data["0"].cidade.tid["0"].value;

      this.breadcrumb1=navParams.data["0"].cidade.name["0"].value;
    }

    if(navParams.data["0"].page.tid["0"].value==0){
      this.categoriaTid='all';
    }else{

      this.categoriaTid=navParams.data["0"].page.tid["0"].value;

      this.breadcrumb2= navParams.data["0"].page.name["0"].value;
    }

    this.url=this.url+'/'+this.cidadeTid+'/'+this.categoriaTid;

    this.http.get(this.url)
    .map(res => res.json())
    .subscribe(data => {
      this.materias = data;
      if(this.materias.length==0){//mostra mensagem de nao ter noticias
        this.HasMaterias=false;
      }
      loading.dismiss();//FECHA O LOADING
    });
  }
}
