import { Component, ViewChild } from '@angular/core';
import { Nav, NavController, LoadingController, Slides, AlertController, Events, NavParams, Platform, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../../pages/tabs/tabs';
import 'rxjs/add/operator/map';
import { MateriaPage } from '../../pages/materia/materia';
import { GetCidadePage } from '../../pages/get-cidade/get-cidade';
import { OfflinePage } from '../../pages/offline/offline';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { DomSanitizer } from '@angular/platform-browser'
import * as $ from 'jquery'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {

  @ViewChild(Slides) slides: Slides;

  @ViewChild('destaques') slidesDestaques: Slides;

  @ViewChild('ultimas') slidesUltimas: Slides;

  @ViewChild('destaquesEsportes') slidesEsportes: Slides;

  @ViewChild('destaquesEconomia') slidesEconomia: Slides;

  @ViewChild('videos') slidesVideos: Slides;

  public materias_slide_topo: any;

  public materias_slide_topo2: any;//3 matérias abaixo do slide

  public materias_destaque: any;

  public materias_destaque_categorias: any;

  public materias_destaque_semana: any;

  public materias_cotidiano: any;

  public materias_economia: any;

  public materias_esportes: any;

  public materias_variedades: any;

  public materias_Ultimas: any;

  materiapage = MateriaPage;

  errorPage = OfflinePage;

  private url: string = "http://live-topuai.pantheonsite.io/rest/materias";

  private url_cotidiano: string = "http://live-topuai.pantheonsite.io/rest/materias8";
  //'46', '14', '13', '44'
  private url_esportes: string = "http://live-topuai.pantheonsite.io/rest/materias8";
  //6
  private url_variedades: string = "http://live-topuai.pantheonsite.io/rest/materias8";
  //'47', '7', '45'
  private url_economia: string = "http://live-topuai.pantheonsite.io/rest/materias8";
  //42,18,43
  private url_home: string = "http://live-topuai.pantheonsite.io/rest/materias_home";

  private urlDestaques: string = "http://live-topuai.pantheonsite.io/rest/materias-destaques";

  private urlDestaquesInterna: string = "http://live-topuai.pantheonsite.io/rest/materias-destaques-interna";

  private urlDestaquesSecundarioInterna: string = "http://live-topuai.pantheonsite.io/rest/materias-destaques-interna-secundarios";

  private urlDestaquesSecundarioHome: string = "http://live-topuai.pantheonsite.io/rest/materias-destaques-secundarios";

  private urlDestaquesSemana: string = "http://live-topuai.pantheonsite.io/rest/destaques_semana";

  private urlUltimas: string = "http://live-topuai.pantheonsite.io/rest/ultimas_noticias";

  private videosDestaque: string = "http://live-topuai.pantheonsite.io/rest/videos/destaque";

  public videosData: any;

  public cidade: any;

  public cidadeId:string = "";

  public parametros: any;

  public isHome: any;

  public isOnlyCidade: any;

  public editoria: any;

  public editoriaId:string = "";

  public breadcrumb1:string = "";

  public breadcrumb2:string = "";

  public classScroll: any;

  public loading: any;

  constructor(public navCtrl: NavController,
              public http: Http,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public nav: Nav,
              public alertCtrl: AlertController,
              public events: Events,
              public params: NavParams,
              private sanitized: DomSanitizer,
              public platform: Platform,
              private ga: GoogleAnalytics,
              public modalCtrl: ModalController) {

    this.parametros=params;
    this.isHome=0;
    this.isOnlyCidade=0;

    this.loading = this.loadingCtrl.create({});
    this.loading.present();

    this.storage.get('cidade').then((val) => {//PEGA A CIDADE SELECIONADA

      if((typeof(this.parametros.data["0"]) != "undefined" &&
         typeof(this.parametros.data["0"].filter_categoria) != "undefined") &&
         (this.parametros.data["0"].filter_categoria==1)){
          if(this.parametros.data["0"].cidade==null){//sem cidade,apenas categorias
            this.url=this.url+'/all/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlDestaques=this.urlDestaques+'/all/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlDestaquesSemana=this.urlDestaquesSemana+'/all/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlUltimas=this.urlUltimas+'/all/'+this.parametros.data["0"].page.tid["0"].value;
            this.breadcrumb2=this.parametros.data["0"].page.name["0"].value;
            this.cidadeId='';
            this.editoriaId=this.parametros.data["0"].page.tid["0"].value;
            console.log(1);
          }else{
            this.editoria=true;
            this.url=this.url+'/'+this.parametros.data["0"].cidade.tid["0"].value+'/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlDestaques=this.urlDestaques+'/'+this.parametros.data["0"].cidade.tid["0"].value+'/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlDestaquesSemana=this.urlDestaquesSemana+'/'+this.parametros.data["0"].cidade.tid["0"].value+'/'+this.parametros.data["0"].page.tid["0"].value;
            this.urlUltimas=this.urlUltimas+'/'+this.parametros.data["0"].cidade.tid["0"].value+'/'+this.parametros.data["0"].page.tid["0"].value;
            this.breadcrumb1=this.parametros.data["0"].cidade.name["0"].value;
            this.breadcrumb2=this.parametros.data["0"].page.name["0"].value;
            this.urlDestaquesSecundarioHome=this.urlDestaquesSecundarioInterna+'/'+this.parametros.data["0"].cidade.tid["0"].value+'/'+this.parametros.data["0"].page.tid["0"].value;
            this.classScroll=true;
            this.cidadeId=this.parametros.data["0"].cidade.tid["0"].value;
            this.editoriaId=this.parametros.data["0"].page.tid["0"].value;
            this.isHome=1;
            this.isOnlyCidade=1;
            console.log(2);
          }
      }else if((typeof(this.parametros.data["0"]) != "undefined" && typeof(this.parametros.data["0"].topuai) != "undefined")&&this.parametros.data["0"].topuai==1){//home topuai
        this.breadcrumb1="TopUai";
        this.classScroll=true;
        this.url=this.url_home;
        this.url_cotidiano=this.url_cotidiano+'/all/46,14,13,44';
        this.url_esportes=this.url_esportes+'/all/6';
        this.url_variedades=this.url_variedades+'/all/47,7,45';
        this.url_economia=this.url_economia+'/all/42,18,43';
        this.cidadeId='';
        this.editoriaId='';
        this.isHome=1;
        console.log(3);
      }else if((typeof(this.parametros.data["0"]) != "undefined" && typeof(this.parametros.data["0"].only_cidade) != "undefined")&&this.parametros.data["0"].only_cidade==1){

        this.url=this.url+'/'+this.parametros.data["0"].page.tid["0"].value+'/all';
        this.urlDestaques=this.urlDestaquesInterna;
        this.urlDestaquesSecundarioHome=this.urlDestaquesSecundarioInterna+'/'+this.parametros.data["0"].page.tid["0"].value+'/all';
        this.urlDestaques=this.urlDestaques+'/'+this.parametros.data["0"].page.tid["0"].value+'/all';
        this.urlDestaquesSemana=this.urlDestaquesSemana+'/'+this.parametros.data["0"].page.tid["0"].value+'/all';
        this.urlUltimas=this.urlUltimas+'/'+this.parametros.data["0"].page.tid["0"].value+'/all';
        this.url_cotidiano=this.url_cotidiano+'/'+this.parametros.data["0"].page.tid["0"].value+'/46,14,13,44';
        this.url_esportes=this.url_esportes+'/'+this.parametros.data["0"].page.tid["0"].value+'/6';
        this.url_variedades=this.url_variedades+'/'+this.parametros.data["0"].page.tid["0"].value+'/47,7,45';
        this.url_economia=this.url_economia+'/'+this.parametros.data["0"].page.tid["0"].value+'/42,18,43';
        this.breadcrumb1=this.parametros.data["0"].page.name["0"].value;
        this.cidadeId=this.parametros.data["0"].page.tid["0"].value;
        this.editoriaId='';
        this.cidade=true;
        this.classScroll=true;
        this.isHome=1;
        this.isOnlyCidade=1;
        console.log(4);
      }else{
        if(val!=null&&val.name["0"].value!='Todas'){//MONTA URL COM CIDADE
          this.url=this.url+'/'+val.tid["0"].value;
          this.urlDestaques=this.urlDestaquesInterna;
          this.urlDestaquesSecundarioHome=this.urlDestaquesSecundarioInterna+'/'+val.tid["0"].value;
          this.urlDestaques=this.urlDestaques+'/'+val.tid["0"].value;
          this.urlDestaquesSemana=this.urlDestaquesSemana+'/'+val.tid["0"].value;
          this.urlUltimas=this.urlUltimas+'/'+val.tid["0"].value;
          this.breadcrumb1=val.name["0"].value;
          this.url_cotidiano=this.url_cotidiano+'/'+val.tid["0"].value+'/46,14,13,44';
          this.url_esportes=this.url_esportes+'/'+val.tid["0"].value+'/6';
          this.url_variedades=this.url_variedades+'/'+val.tid["0"].value+'/47,7,45';
          this.url_economia=this.url_economia+'/'+val.tid["0"].value+'/42,18,43';
          this.cidadeId=val.tid["0"].value;
          this.editoriaId='';
          this.cidade=true;
          this.classScroll=true;
          this.isHome=1;
          this.isOnlyCidade=1;
          console.log(5);
        }else{//sem cidade
          this.presentProfileModal();
          this.url=this.url_home;
          this.cidadeId='';
          this.editoriaId='';
          this.url_cotidiano=this.url_cotidiano+'/all/46,14,13,44';
          this.url_esportes=this.url_esportes+'/all/6';
          this.url_variedades=this.url_variedades+'/all/47,7,45';
          this.url_economia=this.url_economia+'/all/42,18,43';
          this.isHome=1;
          console.log(6);
        }
      }

      this.getContent();//chama o conteudo

      // this.presentConfirm();

      if(typeof(this.parametros.data.goTOmateria) != "undefined" && this.parametros.data.goTOmateria==1){//ve se tem redirect para outra matéria
        platform.ready().then(() => {
          this.loading.dismiss().catch(() => {});//FECHA O LOADING
          this.nav.push(MateriaPage,this.parametros.data.materia);
        });
      }
      if(typeof(this.parametros.data.materia_nid) != "undefined" ){//ve se tem redirect de Push notification
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('push');
        this.navCtrl.push(this.materiapage,{materia_nid:this.parametros.data.materia_nid});// abre materia de push
      }
    });
  }//fim do controller



presentConfirm() {
  let alert = this.alertCtrl.create({
    title: 'Confirm purchase',
    message: 'Do you want to buy this book?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Buy',
        handler: () => {
          this.navCtrl.push(MateriaPage,{materia_nid:'536'});
        }
      }
    ]
  });
  alert.present();
}



  getContent(){


      this.http.get(this.urlDestaques)
        .map(res => res.json())
        .subscribe(
          data => {
            var aux_materias_slide_topo=[];
            var aux_materias_slide_topo2=[];
            for (var i = 0; i < data.length; i++) {
              if(i<3){
                aux_materias_slide_topo.push(data[i]);
              }else{
                aux_materias_slide_topo2.push(data[i]);
              }
            }
            this.materias_slide_topo=aux_materias_slide_topo;//SETA AS MATÉRIAS DO SLIDE SUPERIOR
            this.materias_slide_topo2=aux_materias_slide_topo2;//SETA AS MATÉRIA SEGUINTES AO BANNER SUPERIOR
          },
          error => {
            //FECHA O LOADING
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
            console.log('errorPage -1');
            this.nav.setRoot(this.errorPage);
          }
      );


      this.http.get(this.urlDestaquesSecundarioHome)
        .map(res => res.json())
        .subscribe(
          data => {
            this.materias_slide_topo2=data;//SETA AS MATÉRIA SEGUINTES AO BANNER SUPERIOR
          },
          error => {
            //FECHA O LOADING
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
            console.log('errorPage -1');
            this.nav.setRoot(this.errorPage);
          }
      );



      if(this.isHome==1){//se é home
        this.getContent_editorias();
      }else{
        this.http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {

          this.materias_destaque=data;
          if(this.materias_destaque.length==0){
            this.materias_destaque=null;
          }
          var dict= [];
          var dict2= [];

          if(this.materias_destaque!=null){

            for (var i = 0; i < this.materias_destaque.length; i++) {
              if(typeof(this.materias_destaque[i].field_category) != "undefined"){
                if(!Array.isArray(dict[this.materias_destaque[i].field_category])){
                  dict[this.materias_destaque[i].field_category]=[];
                }
                dict[this.materias_destaque[i].field_category].push(this.materias_destaque[i]);
              }
            }
            for (var key in dict) {
                dict2.push(dict[key]);
              }
          }

          this.materias_destaque_categorias=dict2;
          this.loading.dismiss().catch(() => {});//FECHA O LOADING

          },
          error => {
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
            console.log('errorPage -2');
            this.nav.setRoot(this.errorPage);
          }
        );
      }

      //inicio destaques da semana
      this.http.get(this.urlDestaquesSemana)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_destaque_semana=data;
          if(this.materias_destaque_semana.length==0){
            this.materias_destaque_semana=null;
          }
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim destaques da semana

      //inicio Ultimas Noticias
      this.http.get(this.urlUltimas)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_Ultimas=data;

      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim Ultimas Noticias
      //
      //Incio videos destaques

       this.http.get(this.videosDestaque)
        .map(res => res.json())
        .subscribe(
          data => {
            this.videosData=Array.from(data);//SETA AS MATÉRIA SEGUINTES AO BANNER SUPERIOR

            for (var i = 0; i < this.videosData.length; i++) {
              this.videosData[i].field_video_embed=this.sanitized.bypassSecurityTrustHtml(this.videosData[i].field_video_embed);
            }
            console.log(this.videosData);
          },
          error => {
            //FECHA O LOADING
            this.loading.dismiss().catch(() => {});//FECHA O LOADING
            console.log('errorPage -1');
            this.nav.setRoot(this.errorPage);
          }
      );



      //Fim videos Destaques


      var cidade_aux=this.cidadeId;
      if(cidade_aux==''){
        cidade_aux='all';
      }
      var editoria_aux=this.editoriaId;
      if(editoria_aux==''){
        editoria_aux='all';
      }
      var randN= Math.floor((Math.random()*6)+1000);

      setTimeout(function(){
        //Zona ML-B. Mobile Leaderboard Capa 1
        $('.zone27').empty().append("<iframe id='a9bf4291' name='a9bf4291' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=27&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=aa8e6828&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=27&amp;cb="+randN+"&amp;n=aa8e6828&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
        //Zona AB-B. Arroba Capa 2
        $('.zone28').empty().append("<iframe id='a57fe4fb' name='a57fe4fb' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=28&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a914f46a&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=28&amp;cb="+randN+"&amp;n=a914f46a&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
        //Zona ML-D. Mobile Leaderboard Capa 2
        $('.zone282').empty().append("<iframe id='a1e6e617' name='a1e6e617' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=28&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a21d8c2b&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=28&amp;cb="+randN+"&amp;n=a21d8c2b&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
        // Zona AB-B. Arroba Capa 2
        $('.zone18').empty().append("<iframe id='a622a231' name='a622a231' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=18&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='300' height='250'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a7dccd30&amp;cb="+randN+"&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=18&amp;cb="+randN+"&amp;n=a7dccd30&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");
        //Zona ML-A. Mobile Leaderboard Rodapé
        $('.zone15').empty().append("<iframe id='a0938ba7' name='a0938ba7' src='http://www.topuai.com.br/banners/www/delivery/afr.php?zoneid=15&amp;cb="+randN+"&amp;front=1&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' frameborder='0' scrolling='no' width='320' height='50'><a href='http://www.topuai.com.br/banners/www/delivery/ck.php?n=a30e37f0&amp;cb="+randN+"r&amp;front=1&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' target='_blank'><img src='http://www.topuai.com.br/banners/www/delivery/avw.php?zoneid=15&amp;cb="+randN+"&amp;front=1&amp;n=a30e37f0&amp;cidade="+cidade_aux+"&amp;editoria="+editoria_aux+"' border='0' alt='' /></a></iframe>");

      }, 1200);

      this.loading.dismiss().catch(() => {});//FECHA O LOADING

  }

  getContent_editorias(){

    //inicio Cotidiano
      this.http.get(this.url_cotidiano)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_cotidiano=data;
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim Cotidiano

      //inicio Esportes
      this.http.get(this.url_esportes)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_esportes=data;
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim Esportes

      //inicio Economia
      this.http.get(this.url_economia)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_economia=data;
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim Economia

      //inicio Variedades
      this.http.get(this.url_variedades)
      .map(res => res.json())
      .subscribe(
        data => {
          this.materias_variedades=data;
      },
      error => {
        //FECHA O LOADING
        this.loading.dismiss().catch(() => {});//FECHA O LOADING
        console.log('errorPage -1');
        this.nav.setRoot(this.errorPage);
      });
      //fim Variedades

  }


  doRefresh(refresher) {
    // console.log('Begin async operation', refresher);
    this.getContent();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewDidEnter() {
    this.ga.trackView('Home - '+this.breadcrumb1);//google analytics
  }
  nextSlide(self: any = this): void {
    self.slides.slidePrev();
  }
  prevSlide(self: any = this): void {
    self.slides.slideNext();
  }
  nextSlideDestaques(self: any = this): void {
    self.slidesDestaques.slidePrev();
  }
  prevSlideDestaques(self: any = this): void {
    self.slidesDestaques.slideNext();
  }
  nextSlideUltimas(self: any = this): void {
    self.slidesUltimas.slidePrev();
  }
  prevSlideUltimas(self: any = this): void {
    self.slidesUltimas.slideNext();
  }
  nextSlideEconomia(self: any = this): void {
    self.slidesEconomia.slidePrev();
  }
  prevSlideEconomia(self: any = this): void {
    self.slidesEconomia.slideNext();
  }
  nextSlideEsportes(self: any = this): void {
    self.slidesEsportes.slidePrev();
  }
  prevSlideEsportes(self: any = this): void {
    self.slidesEsportes.slideNext();
  }
  nextSlideVideos(self: any = this): void {
    self.slidesVideos.slidePrev();
  }
  prevSlideVideos(self: any = this): void {
    self.slidesVideos.slideNext();
  }
  presentProfileModal() {

    let profileModal = this.modalCtrl.create(GetCidadePage);
    profileModal.onDidDismiss(data => {
      if(data.modal=='reload'){
        this.nav.setRoot(TabsPage);
      }
    });
    profileModal.present();
   }
}



