import { Component } from '@angular/core';

import { NavController, LoadingController, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  formContato = {
    nome: '',
    tel: '',
    email: '',
    mensagem: ''
  };

  constructor(public navCtrl: NavController, public toastCtrl: ToastController,public loadingCtrl: LoadingController) {

  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Mensagem enviada com sucesso!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  contatoFormSend(){
    let loading = this.loadingCtrl.create({});
    loading.present();
    loading.dismiss();
    this.presentToast();

  }
}
