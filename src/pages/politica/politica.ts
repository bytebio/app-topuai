import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the PoliticaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-politica',
  templateUrl: 'politica.html'
})
export class PoliticaPagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {}

  closeDialog(){
    this.viewCtrl.dismiss({'modal':'reload'});
  }

}
